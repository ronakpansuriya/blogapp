from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save
import string 
from django.utils.text import slugify 
import random 

# Create your models here.
class Category(models.Model):
    category = models.CharField(max_length=20)
    
    def __str__(self):
        return self.category
    

class Blog(models.Model):
    bloguser = models.ForeignKey(User, on_delete=models.CASCADE)
    blogcategory = models.ForeignKey(Category, on_delete= models.CASCADE, default="", null=True , blank=True)
    slug = models.SlugField(max_length=50, null=True, blank=True, default="")
    publish = models.DateTimeField(auto_now=True)
    blogtitle = models.CharField(max_length=20)
    blogimage = models.ImageField(upload_to='images/',default="" ,null=True, blank=True)
    blogdecription = models.TextField()

    def __str__(self):
        return self.blogtitle

    # def __slug__(self):
    #     return slugify(self.blogtitle)


class Contact(models.Model):
    name = models.CharField(max_length=50)
    l_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=250)
    number = models.CharField(max_length=10)


class Signup(models.Model):
    name = models.CharField(max_length=20)
    l_name = models.CharField(max_length=20)
    email = models.EmailField(max_length=250)
    password = models.CharField(max_length=100)
    

class Comment(models.Model):
    com = models.CharField(max_length=500)

    def __str__(self):
        return self.com


# class Profile(models.Model):
#     user = models.ForeignKey(Signup, on_delete=models.CASCADE, default="1")
#     name = models.CharField(max_length=50, default="")
#     l_name = models.CharField(max_length=50, default="")
#     email = models.CharField(max_length=200, default="")


#slug auto genreate by using signal 
def random_string_generator(size = 10, chars = string.ascii_lowercase + string.digits): 
    return ''.join(random.choice(chars) for _ in range(size)) 
  
def unique_slug_generator(instance, new_slug = None): 
    if new_slug is not None: 
        slug = new_slug 
    else: 
        slug = slugify(instance.blogtitle) 
    Klass = instance.__class__ 
    qs_exists = Klass.objects.filter(slug = slug).exists() 
      
    if qs_exists: 
        new_slug = "{slug}-{randstr}".format( 
            slug = slug, randstr = random_string_generator(size = 4)) 
              
        return unique_slug_generator(instance, new_slug = new_slug) 
    return slug 

def pre_save_receiver(sender, instance, *args, **kwargs): 
   if not instance.slug: 
       instance.slug = unique_slug_generator(instance) 

pre_save.connect(pre_save_receiver, sender = Blog)   