from django.urls import path
from . import views


urlpatterns = [
    path('search', views.search, name='search'),
    path('index', views.index, name='index'),
    path('', views.home, name='home'),
    path('blog', views.latestblog, name='blog'),
    path('category', views.category, name='category'),
    path('category1', views.category1, name='category1'),
    path('contact', views.contact, name='contact'),
    path('signup', views.signup, name='signup'),
    path('create', views.createblog, name='create'),
    path('continue/<blog_id>', views.continuereading, name='continue'),
    path('continue1', views.continue1, name='continue1'),
    # path('login', views.login,  name='login'),
    # path('logout', views.logout, name='logout'),
]

