
from django.contrib import admin
from .models import Blog, Category, Contact, Signup, Comment
# Register your models here.

class AdminContact(admin.ModelAdmin):
    list_display = ('name','l_name','email','number')


class AdminBlog(admin.ModelAdmin):
    list_display = ('blogcategory', 'blogtitle', 'blogimage', 'blogdecription', 'bloguser')


admin.site.register(Blog,AdminBlog)
admin.site.register(Category)
admin.site.register(Signup)
admin.site.register(Contact,AdminContact)
admin.site.register(Comment)
# admin.site.register(Profile)