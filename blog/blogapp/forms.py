from django.forms import ModelForm
from .models import Contact, Signup, Blog, Comment


class Createcontact(ModelForm):
    class Meta:
        model = Contact
        fields = ['name','l_name','email','number']


class Signup(ModelForm):
    class Meta:
        model = Signup
        fields = ['name', 'l_name', 'email', 'password']


class CreateBlog(ModelForm):
    class Meta:
        model = Blog
        fields = ['blogcategory', 'blogtitle', 'blogimage', 'blogdecription', 'bloguser']


# class Comment(ModelForm):
#     class Meta:
#         model = Comment
#         fields = ['com']