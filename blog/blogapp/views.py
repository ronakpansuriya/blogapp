from django.shortcuts import render,redirect
from django.http import HttpRequest,HttpResponse, request
from .models import Blog, Category, Signup, Comment
from .forms import CreateBlog, Createcontact, Signup, CreateBlog
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
# Create your views here.
def index(request):
    return render(request, 'blogapp/index.html')

def home(request):
    try:
        blog = Blog.objects.all()
    except Blog.DoesNotExist:
        blog = None
    context = {
        'blog':blog
    }
    return render(request,'blogapp/home.html',context)


def category(request,):
    cat = Category.objects.all()
    context = {
        'cat': cat,
        }
    return render(request, 'blogapp/category.html',context)


def category1(request):
    cat = Category.objects.all()
    blog=' '
    if request.method == "POST":
        choice = request.POST.get('choice')
        print(choice)
        blog = Blog.objects.filter(blogcategory = choice) 
        print(blog)


    context = {
        'cat': cat,
        'blog': blog
        }
        
    return render(request, 'blogapp/category.html',context) 

def contact(request):
    if request.method == 'POST':
        form = Createcontact(request.POST)
        if form.is_valid():
            form.save()
            return redirect('contact')
    else:
        form = Createcontact()
    context = {
        'form': form
    }
    return render(request,'blogapp/contact.html', context)


def signup(request):
    if request.method == 'POST':
        form = Signup(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = Signup()
    context = {
        'form': form
    }
    return render(request,'blogapp/signup.html',context)

# def login(request):
#     if request.method == "POST":
#         form = AuthenticationForm(request=request, data=request.POST)
#         if form.is_valid():
#             uname = form.cleaned_data['username']
#             upass = form.cleaned_data['password']
#             user = authenticate(username=uname, password=upass)
#             if user is not None:
#                 login(request, user)
#                 return redirect('home')
#     else:
#         form = AuthenticationForm()
#     context = {
#         'form': form
#     }
#     return render(request, 'blogapp/login.html', context)




def search(request):
    try:
        if request.method == "POST":
            query = request.POST['query']
            post = Blog.objects.filter(blogtitle__icontains = query)
    except:
        post = 'None'
    context = {
            'post': post
        }
    return render(request, 'blogapp/search.html',context)

def latestblog(request):
    latest = Blog.objects.all()[::-1]
    context = {
        'latest': latest
    }
    return render(request, 'blogapp/blog.html', context)

def createblog(request):
    if request.method == "POST":
        form = CreateBlog(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = CreateBlog()
    context = {
        'form': form
    }
    return render(request, 'blogapp/createblog.html', context)


# comment = []
# def continuereading(request,blog_id):
#     blog = Blog.objects.get(pk = blog_id)
#     if request.method == "POST":
#         c = request.POST['comment']
#         comment.append(c)
#         print(comment)
#     context = {
#         'comment': comment,
#         'blog': blog
#     }
#     return render(request, 'blogapp/continuereading.html', context)

def continuereading(request,blog_id):
    blog = Blog.objects.get(pk=blog_id)
    # result = Comment.objects.all()
    # if request.method == "POST":
    #     comment = request.POST['comment']
    #     comm = Comment(com=comment)
    #     comm.save()
    #     return redirect('continue')
    
    context = {
        'blog':blog,
        # 'result': result
  
    }
    return render(request, 'blogapp/continuereading.html', context)


def continue1(request):
    result = Comment.objects.all()
    if request.method == "POST":
        comment = request.POST['comment']
        comm = Comment(com=comment)
        comm.save()
        return redirect('continue1')
    
    context = {
        
        'result': result
  
    }
    return render(request, 'blogapp/continuereading.html', context)
